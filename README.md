### Build instructions
    * Run the *init* NPM script
    * Download the icon set from [here](https://dribbble.com/shots/2888226-1800-Free-Minimal-Icon-Pack-20x20)
        - From the icon set, unzip the folder "1800 Icon Pack [20x20]/SVG_white_icons" as "resources/icons/contrib"
    * Run the *build* script