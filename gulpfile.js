const { src, dest } = require('gulp');
const download = require('gulp-download');

async function statics() {
    return src([
        'src/main.js',
        'src/index.html'
    ]).pipe(dest('bin/'));
}

async function init() {
    return download([
        'https://fonts.gstatic.com/s/cutivemono/v8/m8JWjfRfY7WVjVi2E-K9H6RMTm663A.woff2',
        'https://fonts.gstatic.com/s/cutivemono/v8/m8JWjfRfY7WVjVi2E-K9H6RCTm4.woff2',
        'https://fonts.gstatic.com/s/mallanna/v7/hv-Vlzx-KEQb84YaDFwoGTVAVg.woff2',
        'https://fonts.gstatic.com/s/mallanna/v7/hv-Vlzx-KEQb84YaDFw0GTU.woff2',
        'https://fonts.gstatic.com/s/ntr/v7/RLpzK5Xy0ZjSA2Jt1TA.woff2',
        'https://fonts.gstatic.com/s/ntr/v7/RLpzK5Xy0ZjSH2Jt.woff2'
    ]).pipe(dest('resources/fonts/'));
}

module.exports = {
    statics,
    init
}