import React from 'react';
import { Provider as StoreProvider } from 'react-redux';
import { HashRouter as Router, Route, Switch, Redirect, useLocation } from 'react-router-dom';
import ROUTES from '@app/routes';
import { TitleBar } from '@components/title-bar';
import { SideBar } from '@components/side-bar';
import { createStore } from '@app/store';
import { Overlay } from '@containers/overlay';
import { SettingsProvider } from '@containers/settings-provider';

/* Views */
import FlightPlanView from '@views/flight-plan';
import NavaidView from '@views/navaid';
import SettingsView from '@views/settings';
import WrongUrlErrorView from '@views/wrong-url-error';
/* End views */

import './app.scss';

function CurrentRouteTitle() {
    const location = useLocation();

    try {
        const route = Object.values(ROUTES).filter(route => route.path == location.pathname)[0];

        return <h1>{route.title}</h1>;
    } catch {
        return <h1>(Unknown route)</h1>;
    }
}

export function App({ store, ...props }) {
    return (
        <StoreProvider store={store}>
            <Router>
                <header>
                    <TitleBar title="Flight Companion" />
                </header>
                <div>
                    <SideBar />
                    <main>
                        <div className="view">
                            <SettingsProvider>
                                <Switch>
                                    <Route path={ROUTES.fp.path} component={FlightPlanView} />
                                    <Route path={ROUTES.settings.path} component={SettingsView} />
                                    <Route path={ROUTES.navaid.pathFull} component={NavaidView} />

                                    <Route component={WrongUrlErrorView} />
                                </Switch>
                            </SettingsProvider>
                        </div>
                    </main>
                </div>
            </Router>

            <Overlay />
        </StoreProvider>
    );
}

export function createApp() {
    const store = createStore();

    return (props) => App({ ...props, store });
}
