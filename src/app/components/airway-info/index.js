import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { sort, FIX_TYPE_FIX } from '@navdata/airways';
import { getFixDataForAirway } from '@navdata/queries/airways';
import { NavaidMap } from '@components/navaid-map';
import { dmsToString, toDms } from '@app/dms';
import './index.scss';

export function AirwayInfo({ airway }) {
    const navdata = useSelector(({ navdata }) => navdata);
    const fixData = getFixDataForAirway(airway, navdata);
    const mapCentre = fixData.reduce(
        (sum, fix) => [
            sum[ 0 ] + fix.longitude,
            sum[ 1 ] + fix.latitude
        ], [ 0, 0 ])
        .map(x => x / fixData.length);

    return (
        <div className="airway-info">
            <div className="airway-data">
                <b>{ airway.id }</b>
                <table>
                    <tbody>
                        <tr>
                            <th>Fixes</th>
                            <td>
                                <ul>
                                    { fixData.map(fix => <li key={ fix.id }>{ fix.id }</li>) }
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <NavaidMap airways={ [ airway ] } center={ mapCentre } />
        </div>
    )
}
