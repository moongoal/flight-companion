import React, { Children } from 'react';
import classNames from 'classnames';
import './index.scss';

export function ButtonRow({ children, className }) {
    const allClassNames = classNames('button-row', className);

    return (
        <ul className={allClassNames}>
            {
                Children.map(children, child => <li>{child}</li>)
            }
        </ul>
    );
}
