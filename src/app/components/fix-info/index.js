import React, { useState } from 'react';
import { getFixTraitsDescription, isFixEnroute } from '@navdata/fixes';
import { NavaidMap } from '@components/navaid-map';
import { dmsToString, toDms } from '@app/dms';
import './index.scss';

export function FixInfo({ fix }) {
    const { id, latitude, longitude, area, region, traits } = fix;
    const isEnroute = isFixEnroute(fix);

    return (
        <div className="fix-info">
            <div className="fix-data">
                <b>{ id }</b>
                <table>
                    <tbody>
                        <tr>
                            <th>Location</th>
                            <td>{ dmsToString(toDms([ latitude, longitude ])) }</td>
                        </tr>
                        <tr>
                            <th>Area</th>
                            <td>{ isEnroute ? 'Enroute' : area }</td>
                        </tr>
                        <tr>
                            <th>Region</th>
                            <td>{ region }</td>
                        </tr>
                        <tr>
                            <th>Traits</th>
                            <td>
                                <ul>
                                    { getFixTraitsDescription(traits, isEnroute).map((x, i) => <li key={ i }>{ x }</li>) }
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <NavaidMap fixes={ [ fix ] } center={ [ fix.longitude, fix.latitude ] } zoom={ 6 } />
        </div>
    )
}
