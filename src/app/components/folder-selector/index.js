import { remote } from 'electron';
import React from 'react';
import './index.scss';

const dialog = remote.dialog;

function onBrowseClicked(currentFolder, selectFolder) {
    return () => {
        const openDialogProps = {
            title: 'Select folder',
            properties: [
                'openDirectory',
                'openFile',
                'multiSelections',
                'createDirectory'
            ]
        };

        if(currentFolder) {
            openDialogProps['defaultPath'] = currentFolder;
        }

        let folder = dialog.showOpenDialogSync(openDialogProps);

        if(folder !== undefined) {
            folder = folder[0];

            selectFolder(folder);
        }
    };
}

export function FolderSelector({ value, onSelect }) {
    return (
        <div className="folder-selector">
            <span className="folder-selector-path">{value || '(No folder selected)'}</span>
            <button onClick={onBrowseClicked(value, onSelect)}>Browse...</button>
        </div>
    );
}
