import React from 'react';
import './index.scss';

const iconMap = {
    menu: require('@icons/contrib/menu_list [#1527].svg'),
    airplane: require('@icons/contrib/airplane_mode [#1406].svg'),
    map: require('@icons/contrib/map_round [#667].svg'),
    settings: require('@icons/contrib/settings [#1365].svg'),
    tower: require('@icons/contrib/radio_tower [#1108].svg'),
    book: require('@icons/contrib/book [#1207].svg'),
    winter: require('@icons/contrib/winter_is_coming [#1258].svg'),
    road: require('@icons/edit/road_round [#607].svg')
};

export function Icon({ type, title }) {
    const iconContent = iconMap[type];

    if (iconContent === undefined) {
        console.warn(`Unable to find icon type "${type}". Using default empty icon...`);
    }

    return <span title={title} className="icon" dangerouslySetInnerHTML={{__html: iconContent}} />;
}