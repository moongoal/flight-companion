import React, { useState, useLayoutEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import 'ol/ol.css';
import { Map, View } from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import * as proj from 'ol/proj';
import { defaults as defaultControls } from 'ol/control';
import { buildFixLayer, buildFixSource, buildAirwaysLayer } from './layers';

function buildMap(target, center, zoom) {
    return new Map({
        target,
        layers: [
            new TileLayer({
                source: new OSM()
            })
        ],
        view: new View({
            center: proj.fromLonLat(center),
            zoom
        }),
        controls: defaultControls({ attribution: false })
    });
}

export function NavaidMap({ fixes, airways, center, zoom = 1, route = false }) {
    const navdata = useSelector(({ navdata }) => navdata);
    const [ map, setMap ] = useState(null);
    const mapRef = useRef(null);

    useLayoutEffect(
        () => {
                if(!map) {
                    const newMap = buildMap(mapRef.current, center, zoom);

                    if(fixes) {
                        newMap.addLayer(buildFixLayer(fixes));
                    }

                    if(airways) {
                        newMap.addLayer(buildAirwaysLayer(airways, navdata));
                    }

                    setMap(newMap);
                }
        }
    );

    return <div className="navaid-map" ref={ mapRef }></div>;
}
