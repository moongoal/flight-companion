import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Fill from 'ol/style/Fill';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import RegularShape from 'ol/style/RegularShape';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import * as proj from 'ol/proj';
import { getFixDataForAirway } from '@navdata/queries/airways';

export function buildFixSource(fixes) {
    const features = fixes.map(wp => new Feature({
        type: 'geoMarker',
        geometry: new Point(proj.fromLonLat([ wp.longitude, wp.latitude ]))
    }));

    return new VectorSource({ features });
}

export function buildAirwaysSource(airways, navdata) {
    const features = [];

    airways.forEach(awy => {
        const fixData = getFixDataForAirway(awy, navdata);

        fixData.forEach(fix => features.push(
            new Feature({
                type: 'geoMarker',
                geometry: new Point(proj.fromLonLat([ fix.longitude, fix.latitude ]))
            }))
        );
    });

    return new VectorSource({ features });
}

export function buildFixLayer(fixes) {
    const markerStyle = new Style({
        image: new RegularShape({
            points: 3,
            radius: 8,
            fill: new Fill({ color: 'red' })
        })
    });

    const vectorLayer = new VectorLayer({
        source: buildFixSource(fixes),
        style: () => markerStyle
    });

    return vectorLayer;
}

export function buildAirwaysLayer(airways, navdata) {
    const markerStyle = new Style({
        image: new RegularShape({
            points: 4,
            radius: 8,
            fill: new Fill({ color: 'green' })
        })
    });

    const vectorLayer = new VectorLayer({
        source: buildAirwaysSource(airways, navdata),
        style: () => markerStyle
    });

    return vectorLayer;
}
