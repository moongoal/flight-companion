import React, { useState, useEffect } from 'react';
import './index.scss';

export * from './result';

const modeOptionsMapping = {
    // type: optionsForType
    // optionsForType = { value: label }
    'fix': {
        'id-exact': 'Exact ID',
        'id-starts-with': 'ID starts with',
        'id-contains': 'ID contains'
    },

    'airway': {
        'id-exact': 'Exact ID'
    }
};

function ModeOptions({ type }) {
    const options = modeOptionsMapping[ type ];

    return (
        <>
            {
                Object.entries(options).map(([ value, label ]) => <option value={ value } key={ value }>{ label }</option>)
            }
        </>
    );
}

export function NavaidSearch({
    navdata,
    pattern = '',
    type = 'fix',
    mode = 'id-exact',
    onModeChange,
    onTypeChange,
    onPatternChange
}) {
    return (
        <div className="navaid-search">
            <div>
                <label>Search</label>
                <input accessKey="s" autoFocus={true} type="search" id="navaid-search-pattern" placeholder="Es. BORED" value={ pattern } onChange={ e => onPatternChange(e.target.value) }></input>
            </div>

            <div>
                <label>Type</label>
                <select id="navaid-search-type" value={ type } onChange={ e => onTypeChange(e.target.value) }>
                    <option value="fix">Fix</option>
                    <option value="airway">Airway</option>
                </select>
            </div>

            <div>
                <label>Mode</label>
                <select id="navaid-search-mode" value={ mode } onChange={ e => onModeChange(e.target.value) }>
                    <ModeOptions type={ type } />
                </select>
            </div>
        </div>
    );
}
