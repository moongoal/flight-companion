import React from 'react';
import { FixInfo } from '@components/fix-info';
import { AirwayInfo } from '@components/airway-info';
import { toDms, dmsToString } from '@app/dms';
import { isFixEnroute } from '@navdata/fixes';

function SingleResultView({ result }) {
    const { type, data } = result;

    switch (type) {
        case 'fix':
            return <FixInfo fix={ data } />;

        case 'airway':
            return <AirwayInfo airway={ data } />;

        default:
            throw new Error('Invalid navaid result type ' + type);
    }
}

function MultipleResultLine({ result, onResultSelect }) {
    const { type, data } = result;
    const info = { // Output info
        id: data.id,
        location: dmsToString(toDms([ data.latitude, data.longitude ]))
    };

    switch (type) {
        case 'fix':
            info.area = isFixEnroute(data) ? 'Enroute' : data.area;
            break;
    }

    return (
        <tr key={ info.id }>
            <td className="id" onClick={ () => onResultSelect(result) }>{ info.id }</td>
            <td>{ info.frequency }</td>
            <td>{ info.location }</td>
            <td>{ info.area }</td>
        </tr>
    );
}

function MultipleResultView({ results, onResultSelect }) {
    const resultLines = results.map((singleResult, idx) => <MultipleResultLine result={ singleResult } onResultSelect={ onResultSelect } key={ idx } />);

    return (
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Frequency</th>
                    <th>Coordinates</th>
                    <th>Area</th>
                </tr>
            </thead>
            <tbody>
                { resultLines }
            </tbody>
        </table>
    );
}

export function NavaidSearchResult({ result, onResultSelect }) {
    const resultWidget = Array.isArray(result)
        ? <MultipleResultView results={ result } onResultSelect={ onResultSelect } />
        : <SingleResultView result={ result } />;

    return result && (!Array.isArray(result) || (Array.isArray(result) && result.length))
        ? (
            <section className="navaid-search-result">
                <h3>Result</h3>
                { resultWidget }
            </section>
        )
        : <h3>No result</h3>;
}
