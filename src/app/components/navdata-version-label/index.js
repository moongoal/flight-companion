import React from 'react';
import dateFormat from 'dateformat';
import './index.scss';

const DATE_FORMAT = "ddmmmyy";

export function NavdataVersionLabel({ navdata }) {
    const { info } = navdata;
    const { cycle } = info;
    let { validFrom, validTo } = info;

    validFrom = dateFormat(validFrom, DATE_FORMAT).toUpperCase();
    validTo = dateFormat(validTo, DATE_FORMAT).toUpperCase();

    return (
        <table className="navdata-version">
            <tbody>
                <tr>
                    <td>Cycle:</td>
                    <td>{ cycle }</td>
                </tr>
                <tr>
                    <td>Valid from:</td>
                    <td>{ validFrom }</td>
                </tr>
                <tr>
                    <td>valid to:</td>
                    <td>{ validTo }</td>
                </tr>
            </tbody>
        </table>
    );
}
