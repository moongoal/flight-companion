import React, { useLayoutEffect } from 'react';
import './index.scss';

export function Overlay({ title, caption }) {
    return (
        <div className="overlay">
            <div className="overlay-inner">
                <h1>{title}</h1>
                <h2>{caption}</h2>
            </div>
        </div>
    );
}
