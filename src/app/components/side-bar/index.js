import React from 'react';
import { NavLink } from 'react-router-dom';
import { Icon } from '@components/icon';
import ROUTES from '@app/routes';
import './index.scss';

function SideBarLink({ children, ...rest }) {
    const linkProps = {
        ...rest,
        activeClassName: 'active'
    };

    return <NavLink {...linkProps}>{children}</NavLink>;
}

export function SideBar() {
    return (
        <ul className="side-bar">
            <li title="File a flight plan">
                <SideBarLink to={ROUTES.fp.path} replace>
                    <Icon type="airplane" />
                </SideBarLink>
            </li>

            <li title="Map">
                <SideBarLink to={ROUTES.map.path} replace>
                    <Icon type="map" />
                </SideBarLink>
            </li>

            <li title="Aerodrome Info">
                <SideBarLink to={ROUTES.aerodromes.path} replace>
                    <Icon type="road" />
                </SideBarLink>
            </li>

            <li title="NAVAID Info">
                <SideBarLink to={ROUTES.navaid.pathMain} replace>
                    <Icon type="tower" />
                </SideBarLink>
            </li>

            <li title="Weather">
                <SideBarLink to={ROUTES.weather.path} replace>
                    <Icon type="winter" />
                </SideBarLink>
            </li>

            <li title="Pilot Logbook">
                <SideBarLink to={ROUTES.logbook.path} replace>
                    <Icon type="book" />
                </SideBarLink>
            </li>

            <li title="Settings">
                <SideBarLink to={ROUTES.settings.path} replace>
                    <Icon type="settings" />
                </SideBarLink>
            </li>
        </ul>
    )
}
