import React from 'react';
import './index.scss';

export function TitleBar({ title }) {
    return (
        <div className="title-bar">
            <div className="inner">
                {title}
            </div>
        </div>
    );
}