import React, { Children, useEffect } from 'react';
import { connect } from 'react-redux';
import { loadNavdata } from '@app/store';

export function NavdataProviderInner({ settings, navdata, children, loadNavdata }) {
    const navdataPresent = () => navdata !== undefined && navdata['info'] !== undefined;

    useEffect(() => {
        if (!navdataPresent()) {
            if (settings && settings.loaded) {
                loadNavdata();
            }
        }
    });

    if (!navdataPresent()) {
        return null;
    }

    return (
        <>
            {
                Children.map(
                    children,
                    c => React.cloneElement(c, { navdata })
                )
            }
        </>
    );
}

const mapStateToProps = ({ navdata, settings }) => ({ navdata, settings });
const mapDispatchToProps = { loadNavdata };

export const NavdataProvider = connect(mapStateToProps, mapDispatchToProps)(NavdataProviderInner);
