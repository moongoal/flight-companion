import React from 'react';
import { connect } from 'react-redux';
import { Overlay as OverlayComponent } from '@components/overlay';

function OverlayContainer({ overlay, ...props }) {
    if(!overlay.length) {
        return null;
    }
    
    let overlayData = overlay[0].data;

    return <OverlayComponent title={overlayData.title} caption={overlayData.caption} {...props} />;
}

const mapStateToProps = ({ overlay }) => ({
    overlay
});

export const Overlay = connect(mapStateToProps)(OverlayContainer);
