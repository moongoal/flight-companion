import React, { Children, useEffect } from 'react';
import { connect } from 'react-redux';
import { loadSettings, storeSettings, setSimBasePath } from '@app/store';

export function SettingsProviderInner({ settings, children, loadSettings, storeSettings, setSimBasePath }) {
    const settingsPresent = () => settings && settings['loaded'] === true;

    useEffect(() => {
        if (!settingsPresent()) {
            loadSettings();
        }
    });

    if(!settingsPresent()) {
        return null;
    }

    return (
        <>
            {
                Children.map(
                    children,
                    c => React.cloneElement(c, typeof(c.type) !== 'string' ? { settings, storeSettings, loadSettings, setSimBasePath } : undefined)
                )
            }
        </>
    );
}

const mapStateToProps = ({ settings }) => ({ settings });
const mapDispatchToProps = {
    loadSettings,
    setSimBasePath,
    storeSettings,
};

export const SettingsProvider = connect(mapStateToProps, mapDispatchToProps)(SettingsProviderInner);
