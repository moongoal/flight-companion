function decToDms(decValue) {
    const deg = Math.trunc(decValue);
    const decMinSec = Math.abs((decValue - deg) * 60);
    const min = Math.trunc(decMinSec);
    const sec = Math.trunc((decMinSec - min) * 60000) / 1000;

    return [ deg, min, sec ];
}

/**
 * Convert decimal geogrpahical coordinates to DMS.
 * 
 * @param {[Number]} latLon Array containing latitude and longitude to convert to DMS
 * @return The input array with every value converted to a DMS array [deg, min, sec]
 */
export function toDms(latLon) {
    return latLon.map(x => decToDms(x));
}

export function dmsToString(dms) {
    const [ lat, lon ] = dms;

    return `${Math.abs(lat[ 0 ])}° ${lat[ 1 ]}' ${lat[ 2 ]}" ${lat[ 0 ] >= 0 ? 'N' : 'S'} ${Math.abs(lon[ 0 ])}° ${lon[ 1 ]}' ${lon[ 2 ]}" ${lon[ 0 ] >= 0 ? 'E' : 'W'}`;
}
