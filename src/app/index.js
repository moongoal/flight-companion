import React from 'react';
import ReactDOM from 'react-dom';
import { createApp } from './app';
import { supply } from 'mockimocki';
import 'polyfill-object.fromentries';

//supply('mock');

const App = createApp();
const appContainer = document.getElementById("app");

ReactDOM.render(<App />, appContainer);
