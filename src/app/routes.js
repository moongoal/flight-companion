import { generatePath } from 'react-router-dom';

// /**
//  * Parametric route encoding.
//  * 
//  * @param {Object} route Route object
//  * @param {Object} components Parameter components of the route
//  */
// export function pRouteEncode({ path }, params) {
//     return path + '?' + Object.entries(params).map(([key, value]) => key + '=' + encodeURIComponent(value)).join('&');
// }

// /**
//  * Decode a route path into its path + parameters.
//  * 
//  * @param {String} path Route path to decode
//  * @returns {{path: String, params: [String]}} The decoded route path
//  */
// export function pRouteDecode(path) {
//     const [ routePath, routeRawParams ] = path.split('?');
//     const routeParams = routeRawParams
//         ? Object.fromEntries(routeRawParams.split('&').map(param => param.split('=', 1)))
//         : {};

//     return { path: routePath, params: routeParams };
// }

let routes = {
    fp: {
        path: '/flight-plan',
        title: 'Flight plan'
    },

    map: {
        path: '/map',
        title: 'Maps'
    },

    aerodromes: {
        path: '/aerodromes',
        title: 'Aerodromes'
    },

    navaid: {
        pathFull: '/navaid',
        pathMain: '/navaid',
    },

    weather: {
        path: '/weather',
        title: 'Weather'
    },

    logbook: {
        path: '/logbook',
        title: 'Logbook'
    },

    settings: {
        path: '/settings',
        title: 'Settings'
    }

};

routes[ 'default' ] = routes[ 'fp' ];

export default routes;
