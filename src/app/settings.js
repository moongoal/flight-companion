import { promises as fs } from 'fs';

export function loadSettingsFromFile(configPath) {
    return fs.readFile(configPath, { encoding: 'utf-8' }).then(
        value => {
            const settings = JSON.parse(value);

            settings['loaded'] = true;

            return settings;
        },
        err => {
            if (err.code == 'ENOENT') {
                console.warn('Settings file not found. Creating new settings file...');
                storeSettingsToFile(configPath, {});
            } else {
                throw err;
            }
        }
    );
}

export function storeSettingsToFile(configPath, settings) {
    const settings2 = {...settings};

    delete settings2['loaded'];
    
    return fs.writeFile(configPath, JSON.stringify(settings2), { encoding: 'utf-8' });
}
