import * as actions from './actions';

export const setSimBasePath = simBasePath => ({
    type: actions.SET_SIM_FOLDER,
    payload: simBasePath
});

export const loadSettings = () => ({
    type: actions.LOAD_SETTINGS
});

export const storeSettings = settings => ({
    type: actions.STORE_SETTINGS,
    payload: settings
});

export const overrideSettings = settings => ({
    type: actions.OVERRIDE_SETTINGS,
    payload: settings
});

export const loadNavdata = () => ({
    type: actions.LOAD_NAVDATA
});

export const overrideNavdata = navdata => ({
    type: actions.OVERRIDE_NAVDATA,
    payload: navdata
});

export const showOverlay = (id, type, data) => ({
    type: actions.SHOW_OVERLAY,
    payload: {
        id,
        type,
        data
    }
});

export const updateOverlay = (id, data) => ({
    type: actions.UPDATE_OVERLAY,
    payload: {
        id,
        data
    }
});

export const hideOverlay = (id) => ({
    type: actions.HIDE_OVERLAY,
    payload: id
});

export const updateNavaidViewPattern = pattern => ({
    type: actions.UPDATE_NAVAID_VIEW_PATTERN,
    payload: pattern
});

export const updateNavaidViewType = type => ({
    type: actions.UPDATE_NAVAID_VIEW_TYPE,
    payload: type
});

export const updateNavaidViewMode = mode => ({
    type: actions.UPDATE_NAVAID_VIEW_MODE,
    payload: mode
});

export const updateNavaidViewResult = result => ({
    type: actions.UPDATE_NAVAID_VIEW_RESULT,
    payload: result
});
