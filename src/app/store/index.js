import { createStore as createReduxStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import initialState from './initial-state';
import rootReducer from './reducers';
import rootSaga from './saga';

export * from './action-creators';

export function createStore() {
    const mwSaga = createSagaMiddleware();
    const store = createReduxStore(
        rootReducer,
        initialState,
        applyMiddleware(mwSaga)
    );

    mwSaga.run(rootSaga);

    return store;
}
