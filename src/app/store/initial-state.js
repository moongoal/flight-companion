export default {
    /* Section for stuff that will be persisted to disk between runs of the application. */
    settings: {
        /* Path to the simulator installation folder */
        simBasePath: null
    },

    /*
        Overlay information;
        
        Overlays are appended to this array and the first is shown.
        When an overlay is hidden, it is removed from this list.
        An empty list means no overlay is displayed.

        Overlay format:
        {
            id, // Randomly generated ID used to recognise the overlay
            type, // Overlay type to show
            data // Information sent to the overlay component
        }
    */
    overlay: [],

    /*
        Navigational database.

        Contents:
        - info: { // Cycle information

        }
    */
    navdata: {
        loaded: false
    },

    navaidView: {
        pattern: '',
        type: 'fix',
        mode: 'id-exact',
        result: null
    }
};
