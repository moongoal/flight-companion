import { combineReducers } from 'redux';
import * as actions from './actions';

function reduceSettings(state = {}, action) {
    switch (action.type) {
        case actions.SET_SIM_FOLDER:
            return {
                ...state,
                simBasePath: action.payload
            };

        case actions.OVERRIDE_SETTINGS:
            return {
                ...action.payload
            };

        default:
            return state;
    }
}

function reduceOverlay(state = [], action) {
    switch (action.type) {
        case actions.SHOW_OVERLAY: {
            let { payload: newOverlay } = action;
            let overlayAlreadyShown = state.filter(x => x.id == newOverlay.id).length > 0;

            if (!overlayAlreadyShown) {
                return [ ...state, newOverlay ];
            } else {
                return state;
            }
        }

        case actions.HIDE_OVERLAY: {
            const targetId = action.payload;
            const newState = state.filter(x => x.id != targetId);

            return (state.length == newState.length) ? state : newState;
        }

        case actions.UPDATE_OVERLAY: {
            const { id, data } = action.payload;

            return state.map(x => {
                if (x.id == id) {
                    x.data = data;
                }

                return x;
            });
        }
    }

    return state;
}

function reduceNavdata(state = {}, action) {
    switch (action.type) {
        case actions.OVERRIDE_NAVDATA:
            return { ...action.payload };

        default:
            return state;
    }
}

function reduceNavaidView(state = {}, action) {
    const { type, payload } = action;

    switch (type) {
        case actions.UPDATE_NAVAID_VIEW_MODE:
            return { ...state, mode: payload };

        case actions.UPDATE_NAVAID_VIEW_TYPE:
            return { ...state, type: payload };

        case actions.UPDATE_NAVAID_VIEW_PATTERN:
            return { ...state, pattern: payload };

        case actions.UPDATE_NAVAID_VIEW_RESULT:
            return { ...state, result: payload };

        default:
            return state;
    }
}

export default combineReducers({
    settings: reduceSettings,
    overlay: reduceOverlay,
    navdata: reduceNavdata,
    navaidView: reduceNavaidView
});
