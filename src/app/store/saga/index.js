import { all } from 'redux-saga/effects';
import settingsRootSaga from './settings';
import navdataRootSaga from './navdata';

export default function* rootSaga() {
    yield all([
        settingsRootSaga(),
        navdataRootSaga()
    ]);
}
