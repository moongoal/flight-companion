import { takeLeading, select, call, put, delay } from 'redux-saga/effects';
import { v4 as uuid } from 'uuid';
import * as actions from '../actions';
import { loadCycleInfo, loadAirwayData, loadFixData } from '@navdata';
import { overrideNavdata, showOverlay as showOverlay, hideOverlay, updateOverlay } from '..';

function* loadNavdata() {
    const overlayId = uuid();
    const { simBasePath } = yield select(({ settings }) => settings);
    const overlayData = {
        title: 'Loading navdata...',
        caption: 'Cycle info'
    };
    const updateLoadingOverlay = caption => updateOverlay(overlayId, { ...overlayData, caption });
    const loadedNavdata = {};
    
    if (simBasePath) {
        yield put(showOverlay(overlayId, 'busy', overlayData));
        loadedNavdata[ 'info' ] = yield call(loadCycleInfo, simBasePath);

        yield put(updateLoadingOverlay("Airways"));
        loadedNavdata[ 'airways' ] = yield call(loadAirwayData, simBasePath);

        yield put(updateLoadingOverlay("Fixes"));
        loadedNavdata[ 'fixes' ] = yield call(loadFixData, simBasePath);

        loadedNavdata.loaded = true;

        yield put(overrideNavdata(loadedNavdata));
        yield put(hideOverlay(overlayId));
    }
}

export default function* rootSaga() {
    yield takeLeading(actions.LOAD_NAVDATA, loadNavdata);
}
