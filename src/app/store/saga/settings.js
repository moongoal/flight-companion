import { put, call, takeLeading, all } from 'redux-saga/effects';
import { v4 as uuid } from 'uuid';
import { loadSettingsFromFile, storeSettingsToFile } from '@app/settings';
import { hideOverlay, showOverlay, overrideSettings } from '../action-creators';
import * as actions from '../actions';

const SETTINGS_FILE_PATH = 'fc.json';

function* loadSettings(action) {
    const overlayId = uuid();
    yield put(showOverlay(overlayId, 'busy', { title: "Loading settings..." }));

    try {
        const loadedSettings = yield call(loadSettingsFromFile, SETTINGS_FILE_PATH);
        yield put(overrideSettings(loadedSettings));
    } finally {
        yield put(hideOverlay(overlayId));
    }
}

function* storeSettings(action) {
    const overlayId = uuid();
    yield put(showOverlay(overlayId, 'busy', { title: "Storing settings..." }));

    try {
        yield call(storeSettingsToFile, SETTINGS_FILE_PATH, action.payload);
    } finally {
        yield put(hideOverlay(overlayId));
    }
}

export default function* () {
    yield all([
        takeLeading(actions.LOAD_SETTINGS, loadSettings),
        takeLeading(actions.STORE_SETTINGS, storeSettings)
    ]);
}
