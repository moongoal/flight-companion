import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { updateNavaidViewMode, updateNavaidViewPattern, updateNavaidViewResult, updateNavaidViewType } from '@app/store';
import { NavdataProvider } from '@containers/navdata-provider';
import { NavdataVersionLabel } from '@components/navdata-version-label';
import { NavaidSearch, NavaidSearchResult } from '@components/navaid-search';
import { search } from '@navdata/search';

function NavaidView({
    navdata,
    navaidView,
    updateNavaidViewResult,
    updateNavaidViewPattern,
    updateNavaidViewType,
    updateNavaidViewMode
}) {
    const [ lastParamChange, setLastParamChange ] = useState(0); // Last param change timestamp
    const { result, type, mode, pattern } = navaidView;
    const clbkChange = (pattern, type, mode) => {
        updateNavaidViewPattern(pattern);
        updateNavaidViewType(type);
        updateNavaidViewMode(mode);
        setLastParamChange(Date.now());
    };
    const onPatternChange = newPattern => clbkChange(newPattern, type, mode);
    const onTypeChange = newType => clbkChange(pattern, newType, mode);
    const onModeChange = newMode => clbkChange(pattern, type, newMode);

    useEffect(() => {
        updateNavaidViewResult(search(navdata, pattern, type, mode));
    }, [ lastParamChange ]);

    return (
        <>
            <h1>Navigation Aids</h1>
            <NavdataProvider>
                <NavdataVersionLabel />
            </NavdataProvider>
            <section>
                <NavdataProvider>
                    <NavaidSearch { ...{ pattern, type, mode, onPatternChange, onTypeChange, onModeChange } } />
                    <NavaidSearchResult result={ result } onResultSelect={ updateNavaidViewResult } />
                </NavdataProvider>
            </section>
        </>
    );
}

const mapStateToProps = ({ navdata, navaidView }) => ({
    navdata,
    navaidView
});

const mapDispatchToProps = {
    updateNavaidViewMode,
    updateNavaidViewType,
    updateNavaidViewPattern,
    updateNavaidViewResult
};

export default connect(mapStateToProps, mapDispatchToProps)(NavaidView);
