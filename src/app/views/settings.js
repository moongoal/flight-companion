import React from 'react';
import { FolderSelector } from '@components/folder-selector';
import { ButtonRow } from '@components/button-row';
import { SettingsProvider } from '@containers/settings-provider';

const LoadSettingsButton = ({ loadSettings }) => <button onClick={loadSettings}>Reload from file</button>;
const StoreSettingsButton = ({ storeSettings, settings }) => { return <button onClick={() => storeSettings(settings)}>Store to file</button> };
const SimFolderSelector = ({ settings, setSimBasePath }) => <FolderSelector value={settings.simBasePath} onSelect={setSimBasePath} />;

export default function () {
    return (
        <>
            <h2>X-Plane</h2>
            <section className="indented">
                <h3>Installation folder</h3>
                <SettingsProvider>
                    <SimFolderSelector />
                </SettingsProvider>
            </section>
            <ButtonRow>
                <SettingsProvider>
                    <LoadSettingsButton />
                    <StoreSettingsButton />
                </SettingsProvider>
            </ButtonRow>
        </>
    );
}
