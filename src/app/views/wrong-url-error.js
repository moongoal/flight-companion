import React from 'react';

export default function WrongUrlErrorView() {
    return (
        <div>
            <p>Sorry, URL doesn't lead anywhere.</p>
            <p>Please report this to the author.</p>
        </div>
    );
}
