const { app, BrowserWindow } = require("electron");

function createMainWindow() {
    let win = new BrowserWindow({
        width: 1600,
        height: 900,
        webPreferences: {
            nodeIntegration: true
        },
        autoHideMenuBar: true,
        frame: false
    });

    win.loadFile("index.html");
}

app.whenReady().then(createMainWindow);
app.on('window-all-closed', () => app.quit());
