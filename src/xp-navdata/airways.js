import { NavdataParser } from './navdata-parser';
import { LineSplitStream } from './utils';

export const SEGMENT_RESTRICTION_FORWARD = 1;
export const SEGMENT_RESTRICTION_BACKWARDS = -1;
export const SEGMENT_RESTRICTION_NONE = 0;

export const FIX_TYPE_FIX = 11;
export const FIX_TYPE_NDB = 2;

/**
 * Any VHF navaid - VOR, TACAN, DME...
 */
export const FIX_TYPE_VHF = 3;

export const AIRWAY_LOW = 1;
export const AIRWAY_HIGH = 2;

function decodeSegmentRestriction(restr) {
    switch (restr) {
        case 'N':
            return SEGMENT_RESTRICTION_NONE; // No restriction

        case 'F':
            return SEGMENT_RESTRICTION_FORWARD; // From begin to end

        case 'B':
            return SEGMENT_RESTRICTION_BACKWARDS; // From end to begin

        default:
            throw new Error(`Invalid airway segment restriction "${restr}".`);
    }
}

export class AirwayDbParser extends NavdataParser {
    constructor(ctx, options) {
        super(ctx, 'earth_awy.dat', options);
    }

    async parse() {
        const rs = this._getStream();
        const lineStream = new LineSplitStream();
        const airways = {};
        const rxpSegment = /^\s*(?<fixBegin>[0-9A-Z]+)\s+(?<fixBeginRegion>[0-9A-Z]+)\s+(?<fixBeginType>\d{1,2})\s+(?<fixEnd>[0-9A-Z]+)\s+(?<fixEndRegion>[0-9A-Z]+)\s+(?<fixEndType>\d{1,2})\s+(?<dirRestriction>[NFB])\s+(?<segType>[12])\s+(?<baseLevel>\d{1,3})\s+(?<topLevel>\d{1,3})\s+(?<awyNames>[0-9A-Z-]+)$/;
        let currentLineNo = 0;

        rs.pipe(lineStream, { end: true });

        for await (let line of lineStream) {
            try {
                currentLineNo++;

                if (currentLineNo < 3 || !line.trim().length) {
                    continue;
                }

                if (line == '99') {
                    break; // End of airway database
                }

                line = line.toUpperCase();

                const mchSegment = rxpSegment.exec(line);
                const fixBegin = {
                    id: mchSegment.groups.fixBegin,
                    region: mchSegment.groups.fixBeginRegion,
                    type: parseInt(mchSegment.groups.fixBeginType)
                };
                const fixEnd = {
                    id: mchSegment.groups.fixEnd,
                    region: mchSegment.groups.fixEndRegion,
                    type: parseInt(mchSegment.groups.fixEndType)
                };
                const segment = {
                    fixBegin, fixEnd,
                    restriction: decodeSegmentRestriction(mchSegment.groups.dirRestriction),
                    type: parseInt(mchSegment.groups.segType),
                    baseLevel: parseInt(mchSegment.groups.baseLevel),
                    topLevel: parseInt(mchSegment.groups.topLevel)
                };
                const awyNames = mchSegment.groups.awyNames.split('-');

                awyNames.forEach(name => {
                    if (airways[ name ] === undefined) {
                        airways[ name ] = { id: name, segments: [ segment ] };
                    } else {
                        airways[ name ].segments.push(segment);
                    }
                });
            } catch (error) {
                console.error(`Error while parsing segment: ${line}.`);

                throw error;
            }
        }

        lineStream.destroy();

        return airways;
    }
}

/**
 * Sort the airway.
 * A well formed airway is assumed.
 * 
 * @param {[Object]} airway The airway to sort
 * @returns {[Object]} The sorted airway
 * 
 */
export function sort(airway) {
    const isFirst = (seg, awy) => awy.find(s => s.fixEnd.id == seg.fixBegin.id) === undefined;
    const nextSegmentIndex = (seg, awy) => awy.findIndex(s => s.fixBegin.id == seg.fixEnd.id);
    const awy = [ ...airway.segments ];
    const res = [];

    // Find first
    for (let i = 0; i < awy.length; ++i) {
        const seg = awy[ i ];

        if (isFirst(seg, awy)) {
            res.push(awy.splice(i, 1)[ 0 ]);
            break;
        }
    }

    // Find subsequent
    while (awy.length) {
        const cur = res[ res.length - 1 ];
        const nextIndex = nextSegmentIndex(cur, awy);
        const next = awy.splice(nextIndex, 1);

        if (next.length === 0) {
            throw new Error(`Cannot sort airway "${airway.id}". Unable to find fix following "${cur.id}" along the route.`);
        }

        res.push(next[ 0 ]);
    }

    return { ...airway, segments: res };
}
