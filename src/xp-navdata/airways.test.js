import { PassThrough } from 'stream';
import {
    AirwayDbParser,
    FIX_TYPE_FIX,
    FIX_TYPE_VHF,
    SEGMENT_RESTRICTION_NONE,
    SEGMENT_RESTRICTION_FORWARD,
    AIRWAY_HIGH
} from './airways';
import { Context } from './context';
import { airwayDbSample } from './mock-data';

describe(AirwayDbParser.name, function () {
    it('should load all the airways', async function () {
        const sAwy = new PassThrough({ encoding: 'utf-8' });
        const parser = new AirwayDbParser(null, { dataStream: sAwy });

        sAwy.end(airwayDbSample);

        const awyData = await parser.parse();

        // All and only airways recorded
        assert.hasAllKeys(awyData, [ 'J13', 'J14', 'J15' ]);

        // Correct IDs
        assert.equal(awyData['J13'].id, 'J13');
        assert.equal(awyData['J14'].id, 'J14');
        assert.equal(awyData['J15'].id, 'J15');

        // Correct # of waypoints
        assert.lengthOf(awyData[ 'J13' ].segments, 3);
        assert.lengthOf(awyData[ 'J14' ].segments, 1);
        assert.lengthOf(awyData[ 'J15' ].segments, 1);

        // Airway beginning & end segments are correct
        assert.sameMembers(awyData[ 'J13' ].segments.map(seg => seg.fixBegin.id), [ 'ABCDE', 'ABC', 'DEF' ]);
        assert.sameMembers(awyData[ 'J14' ].segments.map(seg => seg.fixBegin.id), [ 'DEF' ]);
        assert.sameMembers(awyData[ 'J15' ].segments.map(seg => seg.fixBegin.id), [ 'DEF' ]);

        assert.sameMembers(awyData[ 'J13' ].segments.map(seg => seg.fixBegin.region), [ 'K1', 'K1', 'K2' ]);
        assert.sameMembers(awyData[ 'J14' ].segments.map(seg => seg.fixBegin.region), [ 'K2' ]);
        assert.sameMembers(awyData[ 'J15' ].segments.map(seg => seg.fixBegin.region), [ 'K2' ]);

        assert.sameMembers(awyData[ 'J13' ].segments.map(seg => seg.fixBegin.type), [ FIX_TYPE_FIX, FIX_TYPE_VHF, FIX_TYPE_VHF ]);
        assert.sameMembers(awyData[ 'J14' ].segments.map(seg => seg.fixBegin.type), [ FIX_TYPE_VHF ]);
        assert.sameMembers(awyData[ 'J15' ].segments.map(seg => seg.fixBegin.type), [ FIX_TYPE_VHF ]);

        assert.sameMembers(awyData[ 'J13' ].segments.map(seg => seg.fixEnd.id), [ 'ABC', 'DEF', 'KLMNO' ]);
        assert.sameMembers(awyData[ 'J14' ].segments.map(seg => seg.fixEnd.id), [ 'KLMNO' ]);
        assert.sameMembers(awyData[ 'J15' ].segments.map(seg => seg.fixEnd.id), [ 'KLMNO' ]);

        assert.sameMembers(awyData[ 'J13' ].segments.map(seg => seg.fixEnd.region), [ 'K1', 'K2', 'K2' ]);
        assert.sameMembers(awyData[ 'J14' ].segments.map(seg => seg.fixEnd.region), [ 'K2' ]);
        assert.sameMembers(awyData[ 'J15' ].segments.map(seg => seg.fixEnd.region), [ 'K2' ]);

        assert.sameMembers(awyData[ 'J13' ].segments.map(seg => seg.fixEnd.type), [ FIX_TYPE_VHF, FIX_TYPE_VHF, FIX_TYPE_FIX ]);
        assert.sameMembers(awyData[ 'J14' ].segments.map(seg => seg.fixEnd.type), [ FIX_TYPE_FIX ]);
        assert.sameMembers(awyData[ 'J15' ].segments.map(seg => seg.fixEnd.type), [ FIX_TYPE_FIX ]);

        // Airway restrictions
        assert.sameMembers(awyData[ 'J13' ].segments.map(seg => seg.restriction), [ SEGMENT_RESTRICTION_NONE, SEGMENT_RESTRICTION_NONE, SEGMENT_RESTRICTION_FORWARD ]);
        assert.sameMembers(awyData[ 'J14' ].segments.map(seg => seg.restriction), [ SEGMENT_RESTRICTION_FORWARD ]);
        assert.sameMembers(awyData[ 'J15' ].segments.map(seg => seg.restriction), [ SEGMENT_RESTRICTION_FORWARD ]);

        // Airway types
        assert.sameMembers(awyData[ 'J13' ].segments.map(seg => seg.type), [ AIRWAY_HIGH, AIRWAY_HIGH, AIRWAY_HIGH ]);
        assert.sameMembers(awyData[ 'J14' ].segments.map(seg => seg.type), [ AIRWAY_HIGH ]);
        assert.sameMembers(awyData[ 'J15' ].segments.map(seg => seg.type), [ AIRWAY_HIGH ]);

        // Base levels
        assert.sameMembers(awyData[ 'J13' ].segments.map(seg => seg.baseLevel), [ 180, 180, 180 ]);
        assert.sameMembers(awyData[ 'J14' ].segments.map(seg => seg.baseLevel), [ 180 ]);
        assert.sameMembers(awyData[ 'J15' ].segments.map(seg => seg.baseLevel), [ 180 ]);

        // Top levels
        assert.sameMembers(awyData[ 'J13' ].segments.map(seg => seg.topLevel), [ 450, 450, 450 ]);
        assert.sameMembers(awyData[ 'J14' ].segments.map(seg => seg.topLevel), [ 450 ]);
        assert.sameMembers(awyData[ 'J15' ].segments.map(seg => seg.topLevel), [ 450 ]);
    })
})
