import { NavdataParser } from './navdata-parser';

export class CycleInfoParser extends NavdataParser {
    constructor(ctx, options) {
        super(ctx, 'cycle_info.txt', options);
    }

    async parse() {
        const rs = this._getStream();
        const rxpCycle = /AIRAC cycle\s*:\s*(\d{4})/;
        const rxpValidity = /Valid \(from\/to\)\s*:\s*(\d{2}\/[A-Z]{3}\/\d{4})\s*-\s*(\d{2}\/[A-Z]{3}\/\d{4})/;
        let cycle = null;
        let validFrom = null;
        let validTo = null;
        let buffer = '';

        for await (const chunk of rs) {
            buffer += chunk;

            if (cycle === null) {
                const match = buffer.match(rxpCycle);

                if (match !== null) {
                    // Cycle version information found
                    cycle = parseInt(match[ 1 ]);
                }
            }

            if (validFrom === null) {
                const match = buffer.match(rxpValidity);

                if (match !== null) {
                    // Validity date range found
                    validFrom = new Date(match[ 1 ]);
                    validTo = new Date(match[ 2 ]);
                }
            }

            if (cycle && validFrom) {
                break; // Required data read, no reason to go any further
            }
        }

        return { cycle, validFrom, validTo };
    }
}
