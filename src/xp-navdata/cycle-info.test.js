import { CycleInfoParser } from "./cycle-info";
import { PassThrough } from 'stream';
import { infoDbSample } from './mock-data';

describe(CycleInfoParser.name, () => {
    it('should parse cycle info', async () => {
        const sInfo = new PassThrough({ encoding: 'utf-8' });
        const cycle = 9802;
        const validFrom = '02/FEB/1998';
        const validTo = '04/MAR/1998';
        const cycleInfoParser = new CycleInfoParser(null, { dataStream: sInfo });

        sInfo.push(infoDbSample);
        const parsedInfo = await cycleInfoParser.parse();

        assert.strictEqual(cycle, parsedInfo.cycle);
        assert.strictEqual(new Date(validFrom).valueOf(), parsedInfo.validFrom.valueOf());
        assert.strictEqual(new Date(validTo).valueOf(), parsedInfo.validTo.valueOf());
    })
})
