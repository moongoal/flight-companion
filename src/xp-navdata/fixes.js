/**
 * @module
 * @see http://wiki.flightgear.org/User:Www2/XP11_Data_Specification#FIX
 */

import { NavdataParser } from './navdata-parser';
import { LineSplitStream } from './utils';

/**
 * Check a trait given its position and value.
 * 
 * Position 0, 1 and 2 correspond to columns 27, 28 and 29 respectively.
 * 
 * @param {Number} iTrait The integer fix traits
 * @param {Number} pos The intger position of the trait to check. Must be between 0 and 2
 * @param {String} sTrait Character trait to check. Only the first character in the string is checked
 */
function checkTraitAtPos(iTrait, pos, sTrait) {
    return (iTrait >> (pos * 8) & 0xFF) == sTrait.charCodeAt(0);
}

export function isFixNamedIntersectionAndRnav(fixTraits) {
    return checkTraitAtPos(fixTraits, 0, 'C');
}

export function isFixUnnamedChartedIntersection(fixTraits) {
    return checkTraitAtPos(fixTraits, 0, 'I');
}

export function isFixNdb(fixTraits) {
    return checkTraitAtPos(fixTraits, 0, 'N');
}

export function isFixNamedIntersection(fixTraits) {
    return checkTraitAtPos(fixTraits, 0, 'R');
}

export function isFixVfr(fixTraits) {
    return checkTraitAtPos(fixTraits, 0, 'V');
}

export function isFixRnav(fixTraits) {
    return checkTraitAtPos(fixTraits, 0, 'W');
}

export function isFixArcCenter(fixTraits) {
    return checkTraitAtPos(fixTraits, 0, 'A');
}

export function isFixMiddleMarker(fixTraits) {
    return checkTraitAtPos(fixTraits, 0, 'M');
}

export function isFixOuterMarker(fixTraits) {
    return checkTraitAtPos(fixTraits, 0, 'O');
}

export function isFixFinalApproach(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'A');
}

export function isFixInitialAndFinalApproach(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'B');
}

export function isFixFinalApproachCourse(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'C');
}

export function isFixIntermediateApproach(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'D');
}

export function isFixOffRouteIntersection(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'E') || checkTraitAtPos(fixTraits, 1, 'F');
}

export function isFixInitialApproach(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'I');
}

export function isFixFinalApproachCourseAtInitialApproachFix(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'K');
}

export function isFixFinalApproachCourseAtIntermediateApproachFix(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'L');
}

export function isFixMissedApproach(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'M');
}

export function isFixInitialAndMissedApproach(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'N');
}

export function isFixOceanicEntryPoint(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'O');
}

/**
 * Only for enroute waypoints.
 */
export function isFixPitchAndCatchPoint(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'P');
}

/**
 * Only for terminal waypoints.
 */
export function isFixUnnamedStepdown(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'P');
}

export function isFixSpecialUseAirspace(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'S');
}

/**
 * Only for enroute waypoints.
 */
export function isFixFirOrUirOrControlledAirspaceIntersection(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'U');
}

/**
 * Only for terminal waypoints.
 */
export function isFixNamedStepdown(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'U');
}

export function isFixLatLonIntersectionWithFullDegreeLatitude(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'V');
}

export function isFixLatLonIntersectionWithHalfDegreeLatitude(fixTraits) {
    return checkTraitAtPos(fixTraits, 1, 'W');
}

export function isFixSid(fixTraits) {
    return checkTraitAtPos(fixTraits, 2, 'D');
}

export function isFixStar(fixTraits) {
    return checkTraitAtPos(fixTraits, 2, 'E');
}

export function isFixApproach(fixTraits) {
    return checkTraitAtPos(fixTraits, 2, 'F');
}

/**
 * Return a value stating whether the fix has multiple terminal functions (SID/STAR/approach).
 * 
 * @param {Integer} fixTraits The integer traits
 */
export function isFixMultiple(fixTraits) {
    return checkTraitAtPos(fixTraits, 2, 'Z');
}

/**
 * Return an array of traits for the given fix.
 * 
 * @param {Number} fixTraits Integer fix traits
 * @param {Boolean} isEnroute True for enroute fixes, false for terminal fixes
 */
export function getFixTraitsDescription(fixTraits, isEnroute) {
    const desc = [];

    if (isFixNamedIntersectionAndRnav(fixTraits)) {
        desc.push('Named Intersection and RNAV');
    }

    if (isFixUnnamedChartedIntersection(fixTraits)) {
        desc.push('Unnamed Charted Intersection');
    }

    if (isFixNdb(fixTraits)) {
        desc.push('NDB');
    }

    if (isFixNamedIntersection(fixTraits)) {
        desc.push('Named Intersection');
    }

    if (isFixVfr(fixTraits)) {
        desc.push('VFR Waypoint');
    }

    if (isFixRnav(fixTraits)) {
        desc.push('RNAV Waypoint');
    }

    if (isFixFinalApproach(fixTraits)) {
        desc.push('Final Approach Fix');
    }

    if (isFixInitialAndFinalApproach(fixTraits)) {
        desc.push('Initial and Final Approach Fix');
    }

    if (isFixIntermediateApproach(fixTraits)) {
        desc.push('Intermediate Approach Fix');
    }

    if (isFixOffRouteIntersection(fixTraits)) {
        desc.push('Off-Route Intersection');
    }

    if (isFixInitialApproach(fixTraits)) {
        desc.push('Initial Approach Fix');
    }

    if (isFixFinalApproachCourseAtInitialApproachFix(fixTraits)) {
        desc.push('Final Approach Course Fix at Initial Approach Fix');
    }

    if (isFixFinalApproachCourseAtIntermediateApproachFix(fixTraits)) {
        desc.push('Final Approach Course Fix at Intermediate Approach Fix');
    }

    if (isFixMissedApproach(fixTraits)) {
        desc.push('Missed Approach Fix');
    }

    if (isFixInitialAndMissedApproach(fixTraits)) {
        desc.push('Initial Approach Fix and Missed Approach Fix');
    }

    if (isFixOceanicEntryPoint(fixTraits)) {
        desc.push('Oceanic Entry/Exit Waypoint');
    }

    if (isEnroute && isFixPitchAndCatchPoint(fixTraits)) {
        desc.push('Pitch and Catch Point');
    }

    if (isFixSpecialUseAirspace(fixTraits)) {
        desc.push('Special Use Airspace Waypoint');
    }

    if (isEnroute && isFixFirOrUirOrControlledAirspaceIntersection(fixTraits)) {
        desc.push('FIR/UIR or Controlled Airspace Intersection');
    }

    if (isFixLatLonIntersectionWithFullDegreeLatitude(fixTraits)) {
        desc.push('Latitude/Longitude Intersection (Full Degree of Latitude)');
    }

    if (isFixLatLonIntersectionWithHalfDegreeLatitude(fixTraits)) {
        desc.push('Latitude/Longitude Intersection (Half Degree of Latitude)');
    }

    if (!isEnroute && isFixUnnamedStepdown(fixTraits)) {
        desc.push('Unnamed Stepdown Fix');
    }

    if (!isEnroute && isFixNamedStepdown(fixTraits)) {
        desc.push('Named Stepdown Fix');
    }

    if (isFixSid(fixTraits)) {
        desc.push('SID');
    }

    if (isFixStar(fixTraits)) {
        desc.push('STAR');
    }

    if (isFixApproach(fixTraits)) {
        desc.push('Approach');
    }

    if (isFixMultiple(fixTraits)) {
        desc.push('Multiple (SID/STAR/Approach)');
    }

    return desc;
}

export function isFixEnroute(fix) {
    return fix.area == 'ENRT';
}

export class FixDbParser extends NavdataParser {
    constructor(ctx, options) {
        super(ctx, 'earth_fix.dat', options);
    }

    async parse() {
        const rs = this._getStream();
        const lineStream = new LineSplitStream();
        const fixes = {};
        let currentLineNo = 0;
        const rxpFix = /^\s*(?<lat>[0-9.-]+)\s+(?<lon>[0-9.-]+)\s+(?<id>[0-9A-Z]+)\s+(?<area>[0-9A-Z]+)\s+(?<region>[0-9A-Z]+)\s+(?<traits>\d+)\s*$/;

        rs.pipe(lineStream, { end: true });

        for await (let line of lineStream) {
            try {
                currentLineNo++;

                if (currentLineNo < 3 || !line.trim().length) {
                    continue;
                }

                if (line == '99') {
                    break; // End of airway database
                }

                line = line.toUpperCase();
                const mchField = rxpFix.exec(line);
                const fixId = mchField.groups.id;
                const fixData = {
                    id: fixId,
                    latitude: parseFloat(mchField.groups.lat),
                    longitude: parseFloat(mchField.groups.lon),
                    area: mchField.groups.area, // Airport ICAO code or 'ENRT'
                    region: mchField.groups.region, // ICAO region code
                    traits: parseInt(mchField.groups.traits)
                };

                if (fixes[ fixId ] === undefined) {
                    fixes[ fixId ] = [];
                }

                fixes[ fixId ].push(fixData);
            } catch (error) {
                console.error(`Error while parsing fix: ${line}.`);

                throw error;
            }
        }

        return fixes;
    }
}
