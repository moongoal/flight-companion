import { PassThrough } from 'stream';
import { Context } from './context';
import { FixDbParser, getFixTraitsDescription, isFixEnroute, checkTraitAtPos } from './fixes';
import { fixDbSample } from './mock-data';

describe(FixDbParser.name, function () {
    it('should load all the fixes', async function () {
        const sFix = new PassThrough({ encoding: 'utf-8' });
        const parser = new FixDbParser(null, { dataStream: sFix });

        sFix.end(fixDbSample);

        const fixData = await parser.parse();

        // IDs
        assert.hasAllKeys(fixData, ['AAYRR', 'AAAME']);

        assert.strictEqual(fixData['AAYRR'][0].id, 'AAYRR');
        assert.strictEqual(fixData['AAAME'][0].id, 'AAAME');

        // Areas
        assert.strictEqual(fixData['AAYRR'][0].area, 'KSEA');
        assert.strictEqual(fixData['AAAME'][0].area, 'ENRT');

        // Coordinates
        assert.isTrue(Math.abs(fixData['AAYRR'][0].latitude - 46.646819444) < 2 * Number.EPSILON);
        assert.isTrue(Math.abs(fixData['AAYRR'][0].longitude + 123.722388889) < 2 * Number.EPSILON);

        assert.isTrue(Math.abs(fixData['AAAME'][0].latitude - 37.770908333) < 2 * Number.EPSILON);
        assert.isTrue(Math.abs(fixData['AAAME'][0].longitude + 122.082811111) < 2 * Number.EPSILON);

        // Regions
        assert.strictEqual(fixData['AAYRR'][0].region, 'K1');
        assert.strictEqual(fixData['AAAME'][0].region, 'K2');

        // Traits
        const traits1 = getFixTraitsDescription(fixData['AAYRR'][0].traits, isFixEnroute(fixData['AAYRR'][0]));
        const traits2 = getFixTraitsDescription(fixData['AAAME'][0].traits, isFixEnroute(fixData['AAAME'][0]));

        assert.sameMembers(traits1, ['RNAV Waypoint', 'STAR']);
        assert.sameMembers(traits2, ['RNAV Waypoint', 'STAR']);
    })
})
