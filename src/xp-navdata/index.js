import { PassThrough } from 'stream';
import mockimocki from 'mockimocki';
import { CycleInfoParser } from './cycle-info';
import { AirwayDbParser } from './airways';
import { FixDbParser } from './fixes';
import Context from './context';
import * as mock from './mock-data';

function mockStream(mockData) {
    const stream = new PassThrough({ encoding: 'utf-8' });

    stream.end(mockData);

    return stream;
}

export async function loadCycleInfo(simFolder) {
    const parser = await mockimocki(
        () => new CycleInfoParser(new Context(simFolder)),
        () => new CycleInfoParser(null, { dataStream: mockStream(mock.infoDbSample) })
    );

    return await parser.parse();
}

export async function loadAirwayData(simFolder) {
    const parser = await mockimocki(
        () => new AirwayDbParser(new Context(simFolder)),
        () => new AirwayDbParser(null, { dataStream: mockStream(mock.airwayDbSample) })
    );

    return await parser.parse();
}

export async function loadFixData(simFolder) {
    const parser = await mockimocki(
        () => new FixDbParser(new Context(simFolder)),
        () => new FixDbParser(null, { dataStream: mockStream(mock.fixDbSample) })
    );

    return await parser.parse();
}
