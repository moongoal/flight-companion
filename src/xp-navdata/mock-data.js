export const fixDbSample = `I
Comment containing version information

46.646819444 -123.722388889 AAYRR KSEA K1 4530263
37.770908333 -122.082811111 AAAME ENRT K2 4530263
99
`;

export const airwayDbSample = `I
Comment containing version information

ABCDE K1 11 ABC K1 3 N 2 180 450 J13
ABC K1 3 DEF K2 3 N 2 180 450 J13
DEF K2 3 KLMNO K2 11 F 2 180 450 J13-J14-J15
99
`;

export const infoDbSample = `AIRAC cycle    : 9802
    Version        : 1
    Valid (from/to): 02/FEB/1998 - 04/MAR/1998
`;