import { NavdataParser } from './navdata-parser';

export const NAVAID_TYPE_NDB = 2;
export const NAVAID_TYPE_VOR = 3;
export const NAVAID_TYPE_ILS_LOC = 4;

/**
 * Localizer component of a localizer-only approach.
 */
export const NAVAID_TYPE_LOC = 5;

export const NAVAID_TYPE_ILS_GS = 6;
export const NAVAID_TYPE_ILS_OM = 7;
export const NAVAID_TYPE_ILS_MM = 8;
export const NAVAID_TYPE_ILS_IM = 9;

/**
 * DME component of ILS, VORTAC or VOR-DME.
 */
export const NAVAID_TYPE_DME_COMPONENT = 12;

/**
 * Standalone DME or DME component of an NDB-DME.
 */
export const NAVAID_TYPE_DME = 13;

/**
 * Final approach path alignment point of SBAS/GBAS approach path.
 */
export const NAVAID_TYPE_FAPAP = 14;

/**
 * GBAS differential ground station of a GLS.
 */
export const NAVAID_TYPE_GLS_GBAS_DGS = 15;

/**
 * Landing threshold point or fictious threshold point of an SBAS/GBAS approach.
 */
export const NAVAID_TYPE_TP = 16;

export class NavaidDatabaseParser extends NavdataParser {
    constructor(ctx, options) {
        super(ctx, 'earth_nav.dat', options);
    }

    async parse() {
        const rs = this._getStream();
        const lineStream = new LineSplitStream();
        const navs = {};
        let currentLineNo = 0;

        rs.pipe(lineStream, { end: true });

        for await (let line of lineStream) {
            try {
                currentLineNo++;

                if (currentLineNo < 3 || !line.trim().length) {
                    continue;
                }

                if (line == '99') {
                    break; // End of airway database
                }

                line = line.toUpperCase();
                const fields = line.split(/\s+/);
                let navaid = {
                    type: fields[ 0 ],
                    latitude: parseFloat(fields[ 1 ]),
                    longitude: parseFloat(fields[ 2 ]),
                };

                switch (type) {
                    case NAVAID_TYPE_NDB:
                        navaid = {
                            ...navaid,
                            elevation: parseInt(fields[ 3 ]),
                            frequency: parseInt(fields[ 4 ]),
                            ndbClass: parseInt(fields[ 5 ]),
                            id: fields[ 7 ],
                            area: fields[ 8 ],
                            region: fields[ 9 ],
                            name: fields[ 10 ]
                        };

                        navs[ navaid.id ] = navaid;
                        break;

                    case NAVAID_TYPE_VOR:
                        navaid = {
                            ...navaid,
                            elevation: parseInt(fields[ 3 ]),
                            frequency: parseInt(fields[ 4 ]) / 100,
                            vorClass: parseInt(fields[ 5 ]),
                            slavedVariation: parseInt(fields[ 6 ]),
                            id: fields[ 7 ],
                            region: fields[ 9 ],
                            name: fields[ 10 ]
                        };

                        navs[ navaid.id ] = navaid;
                        break;

                    case NAVAID_TYPE_DME:
                    case NAVAID_TYPE_DME_COMPONENT:
                        navaid = {
                            ...navaid,
                            elevation: parseInt(fields[ 3 ]),
                            frequency: parseInt(fields[ 4 ]) / 100,
                            serviceVolume: parseInt(fields[ 5 ]),
                            bias: parseFloat(fields[ 6 ]),
                            id: fields[ 7 ],
                            area: fields[ 8 ]
                            region: fields[ 9 ],
                            name: fields[ 10 ]
                        };

                        navs[ navaid.id ] = navaid;
                        break;

                    default:
                        console.warning(`Skipping line "${line} as navaid type is not supported."`)
                        continue;
                }
            } catch (error) {
                console.error(`Error while parsing navaid: ${line}.`);

                throw error;
            }
        }

        return navs;
    }
}
