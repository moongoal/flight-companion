import fs from 'fs';
import path from 'path';

export class NavdataParser {
    constructor(ctx, dbFileName, { dataStream } = {}) {
        this._ctx = ctx;
        this._dataStream = dataStream;
        this._dbFileName = dbFileName;
    }

    _getStream() {
        return (
            this._dataStream
            || fs.createReadStream(
                path.join(this._ctx.simFolder, 'Custom Data', this._dbFileName),
                { encoding: 'utf-8' }
            )
        );
    }
}
