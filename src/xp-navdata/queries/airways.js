import { FIX_TYPE_FIX } from '../airways';

/**
 * Extract the unique fixes from an airway.
 * 
 * @param {Object} airway The airway to extract the fix data for
 * @param {Object} navdata The navigation database
 * 
 * @returns An array of fixes
 */
export function getFixDataForAirway(airway, navdata) {
    const { id, segments } = airway;
    const fixes = {};

    segments.forEach(seg => {
        fixes[ seg.fixBegin.id ] = seg.fixBegin;
        fixes[ seg.fixEnd.id ] = seg.fixEnd;
    });

    return Object.entries(fixes).map(
        ([ id, fix ]) => {
            if (fix.type != FIX_TYPE_FIX) {
                return undefined; // TODO: This is because radio aids are not yet loaded. Change it after adding them
            }

            return navdata.fixes[ id ].filter(f => f.region == fix.region)[ 0 ]
        }
    ).filter(x => x);
}
