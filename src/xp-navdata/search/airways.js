function searchAirwayByExactId(airwayId, { airways }) {
    return airways[airwayId.toUpperCase()];
}

export function search(pattern, searchMode, navdata) {
    let result;

    switch(searchMode) {
        case 'id-exact':
            result = searchAirwayByExactId(pattern, navdata);
            break;

        default:
            throw new Error(`Invalid search mode "${searchMode}"`);
    }

    if (!result) {
        return null;
    }

    if (Array.isArray(result)) {
        return result.map(x => ({ type: 'airway', data: x }));
    }

    return { type: 'airway', data: result };
}
