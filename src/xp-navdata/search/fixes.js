import { isFixEnroute } from '../fixes';

export function searchFixByExactId({ fixes }, fixId) {
    return fixes[ fixId.toUpperCase() ];
}

export function searchFixByBeginningOfId({ fixes }, fixPattern) {
    const targetFixId = fixPattern.toUpperCase();

    return fixPattern.length >= 3
        ? Object.entries(fixes).filter(([ id, fix ]) => id.startsWith(targetFixId)).map(([ id, fix ]) => fix)
        : null;
}

export function search(whatFix, mode, navdata) {
    let result;

    switch (mode) {
        case 'id-exact':
            result = searchFixByExactId(navdata, whatFix);
            break;

        case 'id-starts-with':
            result = searchFixByBeginningOfId(navdata, whatFix);
            break;

        default:
            throw new Error(`Unknown search mode "${mode}".`);
    }

    if (!result) {
        return null;
    }

    if (Array.isArray(result)) {
        return result.map(x => ({ type: 'fix', data: x }));
    }

    return { type: 'fix', data: result };
}
