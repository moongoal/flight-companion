import * as fix from './fixes';
import * as awy from './airways';

export function search(navdata, pattern, searchType, searchMode) {
    let result = null;

    if (!navdata.loaded) {
        return null;
    }

    switch (searchType) {
        case 'fix':
            result = fix.search(pattern, searchMode, navdata);
            break;

        case 'airway':
            result = awy.search(pattern, searchMode, navdata);
            break;

        default:
            throw new Error(`Unknown search type "${searchType}".`);
    }

    return result;
}
