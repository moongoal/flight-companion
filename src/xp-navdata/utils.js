import { Transform } from 'stream';

const BUFFER = Symbol('buffer');
const LINES_SPLIT_STREAM_HIGH_WATER_MARK = 16384;

export class LineSplitStream extends Transform {
    constructor({
        readableHighWaterMark = LINES_SPLIT_STREAM_HIGH_WATER_MARK,
        writableHighWaterMark = LINES_SPLIT_STREAM_HIGH_WATER_MARK
    } = {}) {
        super({
            objectMode: true,
            readableHighWaterMark,
            writableHighWaterMark
        });

        this[ BUFFER ] = ''; // Input buffer
    }

    _transform(chunk, encoding, callback) {
        if (encoding === 'buffer') {
            callback(new Error(`Invalid encoding "${encoding}".`));
            return;
        }

        let buff = this[ BUFFER ] + chunk;
        let lines = buff.split('\n');
        let last = lines.pop();

        if (buff.length > 0) { // Remaining text is either part of another line or last line
            this[ BUFFER ] = last;
        } else {
            this[ BUFFER ] = '';
        }

        lines.forEach(line => this.push(line.trim('\r')));
        callback();
    }

    _flush(callback) {
        if (this[ BUFFER ].length > 0) {
            this[ BUFFER ].split('\n').forEach(line => this.push(line.trim('\r')));
            this[ BUFFER ] = '';
        }

        callback();
    }
}
