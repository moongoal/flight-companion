import { LineSplitStream } from './utils';

const LINES_IN_STREAM = 100;
const EOS_TEXT = 'something else';

function newTestStream(withEndingCaret) {
    const text = 'This is some test text.\n';
    const splitStream = new LineSplitStream({ encoding: 'utf-8' });

    for (let i = 0; i < LINES_IN_STREAM; i++) {
        if (!splitStream.write(text)) {
            throw new Error('Not enough room for all samples to be stored.');
        }
    }

    if (!withEndingCaret) {
        splitStream.write(EOS_TEXT);
    }

    splitStream.end();

    return splitStream;
}

describe(LineSplitStream.name, function () {
    it('should transform plain text to lines', async function () {
        const s = newTestStream(true);
        let n = 0;

        for await (const line of s) {
            n++;
        }

        assert.strictEqual(n, LINES_IN_STREAM);
    })

    it('should convert text at the end of the stream even when no caret is present', async function () {
        const s = newTestStream(false);
        let n = 0;
        let lastLine;

        for await (const line of s) {
            n++;
            lastLine = line;
        }

        assert.strictEqual(n, LINES_IN_STREAM + 1);
        assert.strictEqual(lastLine, EOS_TEXT);
    })
})
