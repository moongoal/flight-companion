const path = require('path');

module.exports = {
    entry: path.resolve(__dirname, 'src', 'app', 'index.js'),
    mode: 'development',
    target: 'electron-main',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'bin')
    },
    module: {
        rules: [
            { // Transpile JS
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ["@babel/preset-env", "@babel/react"],
                        plugins: ["@babel/plugin-transform-runtime"]
                    }
                }
            },
            { // Embed Sass CSS
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            { // Embed Plain CSS
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            { // Static files
                test: /\.(woff2|png)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]'
                    }
                }
            },
            { // SVG Icons
                test: /\.svg$/,
                use: {
                    loader: 'svg-inline-loader',
                    options: {
                        removeTags: true,
                        removingTags: ['title', 'desc']
                    }
                }
            }
        ]
    },
    resolve: {
        alias: {
            '@components': path.resolve(__dirname, 'src', 'app', 'components'),
            '@containers': path.resolve(__dirname, 'src', 'app', 'containers'),
            '@fonts': path.resolve(__dirname, 'resources', 'fonts'),
            '@icons': path.resolve(__dirname, 'resources', 'icons'),
            '@node_modules': path.resolve(__dirname, 'node_modules'),
            '@style': path.resolve(__dirname, 'src', 'app', 'style'),
            '@app': path.resolve(__dirname, 'src', 'app'),
            '@views': path.resolve(__dirname, 'src', 'app', 'views'),
            '@navdata': path.resolve(__dirname, 'src', 'xp-navdata')
        }
    }
}
